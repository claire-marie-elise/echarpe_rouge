# Projet Web - Escape Game géographique
## Binôme : Claire-Marie Alla et Elise Frank - Ingénieur 2ème année

Le jeu a été réalisé à l'aide de **MAMP** 4.2.0.23973, **PHP** 7.4.1 et **MYSQL** 5.7.24

### INSTALLATION

1. Télécharger MAMP.
2. Télécharger le projet sur GitLab sous forme de .zip qui s'appellera automatiquement echarpe_rouge-master.zip
3. Dézipper echarpe_rouge-master.zip dans C:/MAMP/htdocs.
4. Ouvrir MAMP.
5. Vérifier dans le logiciel MAMP, en vous rendant dans MAMP, Preferences puis Ports, que le port MySQL est bien le même que celui présent dans script.php à la ligne 3.
6. Dans http://localhost/phpMyAdmin/ créez une nouvelle base de données nommée "lupinbase".
7. Importer le fichier "lupinbase.sql" dans la base de données (en copiant-collant le contenu du fichier dans l'onglet SQL et en cliquant sur Go).
8. Dans votre navigateur (Mozilla ou Chrome de préférence), recopier le lien :  http://localhost/echarpe_rouge-master/ où echarpe_rouge-master correspond au dossier contenant les différents fichiers.
9. Si vous avez envie de mettre ce jeu sur le web, il vous faudra sans doute changer la carte à la ligne 86 du script.js :
<pre><code>L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',maxZoom: 24,
}).addTo(map);
</code></pre>
10. Il ne vous reste plus qu'à jouer !

#### Vous trouverez les solutions du jeu dans le fichier "Solutions_du_jeu-Echarpe_de_soie_rouge.pdf"
