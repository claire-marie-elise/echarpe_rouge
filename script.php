<?php
    /* Relatif à la base de donnée */
    $server = "localhost:3306";
    $user="root";
    $pass="root";
    $bdd="lupinbase";
    $link = mysqli_connect($server, $user, $pass, $bdd);

    /*
    if (!$link) {
      die('Erreur de connexion');
    }*/

    // Récupérer les 3 meilleurs scores et les pseudos pour les afficher
    // dans le "Hall of fame"
    if($_POST['type'] == 'score') {

      $sql1 = "SELECT pseudo, temps FROM scores WHERE temps is not NULL ORDER BY temps LIMIT 3";
      $result = mysqli_query($link, $sql1);
      if($result){
        while($ligne = mysqli_fetch_assoc($result)){
          $tableau[] = $ligne;
        }
        echo json_encode($tableau);
      }
    }

    // Enregistrer le pseudo et le temps de début dans la table 'score'
    elseif($_POST['type'] == 'pseudo') {

      $pseudo = $_POST['txt'];
      $requete = "INSERT INTO scores (pseudo, heure_debut) VALUES ('$pseudo', CURRENT_TIMESTAMP())";
      $result = mysqli_query($link, $requete);
      echo json_encode("");
    }

    // Renvoyer l'objet associé à l'id
    elseif ($_POST['type'] == 'initialisation' || $_POST['type'] == 'id' ){

      $objet = $_POST['id'];
      $sql3 = "SELECT * FROM objets WHERE id =".$objet;
      $result = mysqli_query($link, $sql3);
      if($result){
        while($ligne = mysqli_fetch_assoc($result)){
          // Permet d'avoir un bon retour json
          $ligne['id'] = intval( $ligne['id'] );
          $ligne['type'] = intval( $ligne['type'] );
          $ligne['lat'] = doubleval( $ligne['lat'] );
          $ligne['lng'] = doubleval( $ligne['lng'] );
          $ligne['min_zoom'] = intval( $ligne['min_zoom'] );
          $ligne['bloque_par'] = intval( $ligne['bloque_par'] );
          $ligne['affiche_par'] = intval( $ligne['affiche_par'] );
          $ligne['ouvert'] = intval( $ligne['ouvert'] );
          $tableau[] = $ligne;
        }
        echo json_encode($tableau);
      }
    }

    // Récuperer l'identifiant de l'objet à libérer
    elseif ($_POST['type'] == 'libere'){

      $objetBloquant = $_POST['id'];

      $sql = "SELECT * FROM objets WHERE affiche_par =".$objetBloquant;
      $result = mysqli_query($link, $sql);
      if($result){
        while($ligne = mysqli_fetch_assoc($result)){
          // Permet d'avoir un bon retour json
          $ligne['id'] = intval( $ligne['id'] );
          $ligne['type'] = intval( $ligne['type'] );
          $ligne['lat'] = doubleval( $ligne['lat'] );
          $ligne['lng'] = doubleval( $ligne['lng'] );
          $ligne['min_zoom'] = intval( $ligne['min_zoom'] );
          $ligne['bloque_par'] = intval( $ligne['bloque_par'] );
          $ligne['affiche_par'] = intval( $ligne['affiche_par'] );
          $ligne['ouvert'] = intval( $ligne['ouvert'] );
          $tableau[] = $ligne;
        }
        echo json_encode($tableau);
      }

    }


    // Enregistrer le score du joueur
    elseif ($_POST['type'] == 'fin'){

      $pseudo = $_POST['txt'];
      $requete = "UPDATE scores SET heure_fin=CURRENT_TIMESTAMP(),
                  temps=ABS(TIMEDIFF(heure_debut,CURRENT_TIMESTAMP()))
                  WHERE pseudo ='$pseudo'";
      $result = mysqli_query($link, $requete);
      echo json_encode("");
    }

?>