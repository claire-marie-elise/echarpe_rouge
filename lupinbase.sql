-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 15, 2020 at 01:29 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lupinbase`
--

-- --------------------------------------------------------

--
-- Table structure for table `objets`
--

CREATE TABLE `objets` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `min_zoom` int(11) NOT NULL,
  `bloque_par` int(11) NOT NULL,
  `affiche_par` int(11) NOT NULL,
  `information` varchar(800) NOT NULL,
  `indice` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(800) NOT NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ouvert` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `objets`
--

INSERT INTO `objets` (`id`, `nom`, `type`, `lat`, `lng`, `min_zoom`, `bloque_par`, `affiche_par`, `information`, `indice`, `message`, `icon`, `ouvert`) VALUES
(1, 'Mallette', 40, 48.855733, 2.345114, 15, 11, 0, 'Lupin t’a donné beaucoup de pistes, seulement tu n’as pas tous les éléments…\r\nDu côté de la police, un dossier a été constitué au sujet de l’affaire dans cette mallette. Commence par l’ouvrir ! Le code est sûrement dans les parages.\r\n', '10 Boulevard du Palais', 'Assassinat cette nuit d’une chanteuse de café-concert, retrouvée Rue de Berne à Paris. On sait qu’elle possédait un saphir. Le cadavre montrait la jeune fille étranglée, sa main crispée autour d\'un lambeau de soie rouge, semble-t-il, déchiré. Où est le deuxième morceau ? La femme de chambre est en train d’être interrogée.', 'img/mallette.png', '0'),
(2, 'Boîte à pâtisseries', 20, 48.875573, 2.327484, 18, 0, 3, '« Un homme bien habillé est bien entré dans ma boutique pour acheter des meringues. Il est sans doute allé rejoindre une jolie demoiselle hier ! Avec une tenue pareille, ça se voit que ce type de bonhomme habite dans le 6ème. »', '', '', 'img/boite.png', '0'),
(3, 'Journal déchiré', 20, 48.873069, 2.330443, 18, 0, 4, 'Ce fragment de journal porte la date de la veille et la mention « Journal du soir » : le crime a sans doute eut lieu après neuf heures.\r\nJamais 2 sans 3 ! La troisième pièce à conviction se trouve à la pâtisserie près de de la gare Saint-Lazare.\r\n', '', '', 'img/turf.jpeg', '0'),
(4, 'Encrier', 20, 48.856426, 2.34075, 19, 0, 14, 'Il permettait de faire couler au fond de la Seine un paquet enveloppé dans un numéro du journal du Turf dont les bureaux se trouvent au 33 Boulevard Haussmann.', '', '', 'img/encrier.png', '0'),
(5, 'Clé', 10, 48.856578, 2.340069, 19, 0, 0, '', '', '', 'img/cle.png', '0'),
(6, 'Morceau d\'écharpe', 10, 48.881, 2.3232, 18, 0, 1, '', '', '', 'img/echarpe1.png', '0'),
(7, 'Second morceau d\'écharpe', 30, 48.8713, 2.3199, 20, 6, 15, '', 'L\'écharpe en soie rouge', '<p>Mais tu viens de te faire avoir ! Après avoir inspecté le morceau de l’écharpe que tu viens de lui apporter, Lupin y a trouvé le saphir caché et a pris la fuite avec ! Ce cambrioleur s\'est encore une fois joué de la police et ce n\'est pas aujourd\'hui qu\'il se fera coincer !</p>\r\n<p>Bravo à toi pour avoir résolu l\'affaire ! Si tu n\'es pas trop contrarié par la ruse de Lupin et que tu souhaites en savoir davantage à son sujet, n\'hésite pas à cliquer sur les deux liens dans le bandeau rouge.</p>\r\n<p>Tu peux aussi recommencer l\'enquête en entrant ton pseudo et en cliquant sur \"Démarrer l\'aventure\" pour améliorer ton temps !</p>', 'img/echarpe2.png', '0'),
(8, 'Programme de spectacle', 20, 48.871439, 2.342133, 18, 0, 12, 'Si tu ne veux pas que ton assassin te file entre les doigts, va au rendez-vous proposé par Lupin : 28 décembre, à 10h Rue de Surène avec le morceau d’écharpe que tu as pris sur la victime. Méfie-toi tout de même.', '', '', 'img/programme.png', '0'),
(9, 'Chat', 10, 48.879692, 2.309542, 19, 0, 0, '« Rrroonn...rroonn... »', '', '', 'img/chat.png', '0'),
(10, 'Femme de chambre', 30, 48.8813, 2.322839, 17, 9, 1, '« Oui, je peux vous donner des informations, à condition que vous retrouviez mon chat qui s\'est à nouveau enfui, je sais qu\'il aime bien explorer les espaces verts… »', 'Parc Monceau', '« Ah ! Merci de l\'avoir ramené ! Jenny Saphir, la victime, recevait régulièrement un individu qui restait environ jusqu’à minuit:  \"C’est un homme du monde.\" prétendait–elle, \"Il veut m’épouser.\". Il parait que le corps a été retrouvé par un batelier dans sa péniche près de l’île de la Cité, c’est affreux ! »', 'img/femme.png', '0'),
(11, 'Code mallette', 20, 48.855281, 2.344749, 18, 0, 0, 'La mallette a pour code la date à laquelle elle a été fermée pour la dernière fois, soit le soir du crime.', '', '2811', 'img/code.png', '0'),
(12, 'Porte', 40, 48.854909, 2.341879, 18, 13, 2, 'La porte est verrouillée… il ne te manque plus qu’une pièce à conviction pour entrer et arrêter le suspect !', 'Message par terre', 'Ah il est fort pour ne laisser aucune trace ! Le meurtrier avait tout mis dans son journal et jeté ce dernier à l’eau. Ni une ni deux en prison. Mais tu n’es pas au bout de tes surprises, voilà que son avocat lui trouve un alibi. Il se serait rendu au Théâtre des Variétés le soir du crime. Rien de plus louche !', 'img/porte.png', '0'),
(13, 'Code porte', 20, 48.855036, 2.341713, 19, 0, 0, 'Un homme du monde porte toujours un monocle.', '', 'monocle', 'img/code_porte.png', '0'),
(14, 'Péniche', 30, 48.856224, 2.340957, 18, 5, 10, 'Vous ne pouvez pas rentrer, il vous faut la clé.', 'Chercher sur le quai au nord.', '« Il est bien dommage que certaines pièces à convictions aient été éparpillées par mon chien-chien. J’ai jeté l’encrier près du pont, allez y jeter un œil. »', 'img/peniche.png', '0'),
(15, 'Arsène Lupin', 20, 48.871472, 2.320024, 19, 0, 8, '« Je peux te montrer le deuxième morceau d\'écharpe, portant les traces du suspect. Il s\'est trahi par le sang qu\'il avait sur les doigts. C\'est la dernière pièce à conviction. Parce qu’avant d’étrangler sa pauvre victime, le meurtrier lui a bien donné deux coups de couteau… Il n’y a plus de doutes possibles nous avions le bon suspect. Examine ce deuxième morceau ! »', '', '', 'img/lupin.png', '0');

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(50) NOT NULL,
  `heure_debut` datetime DEFAULT NULL,
  `heure_fin` datetime DEFAULT NULL,
  `temps` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scores`
--

INSERT INTO `scores` (`id`, `pseudo`, `heure_debut`, `heure_fin`, `temps`) VALUES
(1, 'Elise', '2020-12-15 14:11:26', '2020-12-15 14:12:58', '00:01:32'),
(2, 'CMA', '2020-12-15 14:23:58', '2020-12-15 14:25:58', '00:02:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `objets`
--
ALTER TABLE `objets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `objets`
--
ALTER TABLE `objets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `scores`
--
ALTER TABLE `scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
