/* Afficher dans le HTML les 3 meilleurs scores */
charger_text('score', "");

function mise_a_jour_hall_of_fame(result) {

	if (result == null) {
		document.getElementById('1Pseudo').innerHTML = 'seras-tu';
		document.getElementById('1Score').innerHTML = 'le 1er ?';
	}
	else {
		for (let i= 0; i < result.length;i++) {
			p = String(i+1) + 'Pseudo';
			s = String(i+1) + 'Score';
			document.getElementById(p).innerHTML = result[i]['pseudo'];
			document.getElementById(s).innerHTML = result[i]['temps'];
		}
	}

}

function charger_text(type, txt) {

	var data = 'type=' + type + '&txt=' + txt;

	fetch('script.php', {
		method: 'post',
		body: data,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	})
		.then(result => result.json())
		.then(result => {

			if (type == 'score') {
				mise_a_jour_hall_of_fame(result);
			}

	});
}

var map;



/* Récupérer le formulaire */
var jeu = document.getElementById('formulaire');

/* Attendre un évènement de la part de l'utilisateur */
jeu.addEventListener('submit', function(event){
	event.preventDefault();

	/* Récupérer le pseudo de l'utilisateur */
	var pseudo = jeu.elements['pseudo'].value;

	if (pseudo == "") {
		pseudo = 'pseudo';
	}
	charger_text('pseudo', pseudo);

	// Initialisation

	// Liste des objets à sauvegarder
	var sauvegarde = new Array();
	// Liste objets récupérables
	var recuperable = new Array();
	// Créer un feature groupe
	var groupe = new L.featureGroup();

	map = L.map('mapid');
	// Les ajouter à la carte
  groupe.addTo(map);

	/* Charger les objets du début et les sauvegarder */
	charger('initialisation', "1");

	// Condition fin
	var fin = false;

	document.getElementById('fin').style.display = 'none';
	document.getElementById('images').innerHTML = "";
	document.getElementById('images').style.display = 'block';
	// Afficher la carte
	document.getElementById('mapid').style.display = 'block';
	map.setView([48.845, 2.3752], 13);
	L.tileLayer('https://tiles.stadiamaps.com/tiles/osm_bright/{z}/{x}/{y}{r}.png', {
			maxZoom: 20,
			attribution: '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="https://openmaptiles.org/">OpenMapTiles</a> &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
	}).addTo(map);


  /* Contrôler que les objets n'apparaissent qu'à un certain niveau de zoom */
  map.on('zoomend', visibObjet);

  /* Attendre un clique de l'utilisateur sur la map */
  groupe.on('click', onObjectClick);





	/* Charger depuis la base de données un ou des objets grâce à un id */
	function charger(type, id) {

		var data = 'type=' + type + '&id=' + id;

	  fetch('script.php', {
			method: 'post',
			body: data,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		})
	    .then(result => result.json())
	    .then(result => {

				console.log(result)

				if (result != []) {
					for (let i in result) {
						sauvegarde.push(result[i]);
					}
				}

	    })
	}


	/* Ajouter un marqueur à l'objet */
	function marqueur(objet) {

	  // Options de l'icône
	  var iconOptions = {
	    iconUrl: objet.icon
	  }
	  // Creating a custom icon
	  var customIcon = L.icon(iconOptions);

	  // Options du marker
	  var markerOptions = {
			id: objet.id,
	    nom: objet.nom,
			type: objet.type,
			min_zoom: objet.min_zoom,
			bloque_par: objet.bloque_par,
			information: objet.information,
			indice: objet.indice,
			message: objet.message,
			ouvert: objet.ouvert,
	    clickable: true,
	    // déplacement de l'objet avec la souris ou non
	    //draggable: true,
	    icon: customIcon
	  }
	  // Créer le marker
	  var marker = L.marker([objet.lat, objet.lng], markerOptions).addTo(groupe);
	}


	/* Afficher les objets suivant leur niveau de zoom */
	function visibObjet(ev) {

		groupe.clearLayers();

	  /* Parcours de tous les objets pour les integrer au feature groupe */
		for (let i in sauvegarde) {
			if (ev.sourceTarget._zoom >= sauvegarde[i].min_zoom) {
				marqueur(sauvegarde[i]);
			}
		}
	}

	// Afficher un popup
	function popup_text(marker, text, indice) {

		var afficher = '<div><strong>'+marker.options.nom+'</strong></div>'+ text;

		if (indice) {
			bouton_indice = '<div align="right"><input type="button" id="indice" value="indice"/></div>';
			afficher += bouton_indice;
		}
		marker.bindPopup(afficher).openPopup();
	}


	// Cliquer sur le bouton indice
	function click_bouton_indice(marker) {

		var buttonIndice = L.DomUtil.get('indice');
		L.DomEvent.addListener(buttonIndice, 'click',

		function afficher_indice() {
			marker.closePopup();
			popup_text(marker, marker.options.indice);
		});
	}


	/* Rentrer le code dans un formulaire */
	function taper_code(e, code, indexObjet) {

		marker = e.sourceTarget.dragging._marker
		objet = e.sourceTarget.dragging._marker.options;

		// Créer le formulaire en HTML
		var form = '<form id="code" method="post">';
		for (let i = 0; i < code.length; i++) {
			form += '<input id="'+ i +'" type="text" required maxlength="1" size="1">';
		}
		form += '<input id="valider" type="submit" value="OK" /></form>';


		popup_text(marker, objet.information + form, true);
		click_bouton_indice(marker);

		// Ecouter chaque champs de texte
		var input = [];
		for (let i = 0; i < code.length; i++) {
			input.push(L.DomUtil.get(i.toString()));
		}


		// Si l'utilisateur clique sur le bouton, vérifier le formulaire
		var buttonSubmit = L.DomUtil.get('valider');
  	L.DomEvent.addListener(buttonSubmit, 'click',


		function valider(e) {
			e.preventDefault()

			var code_OK = true;

			for (let i = 0; i < code.length; i++) {
				let lettre = code[i].toUpperCase();
				// Peut importe si la lettre est en majuscule ou minuscule
				if (input[i].value.toUpperCase() != lettre) {
					code_OK = false;
				}
			}

			if(code_OK){

				marker.closePopup();
				popup_text(marker, objet.message, false);

				/* afficher les objets suivants */
				if (sauvegarde[indexObjet].ouvert == 0) {
					charger('libere', objet.id);
				}
				sauvegarde[indexObjet].ouvert = 1;
			}

  	});
	}


	/* Rechercher l'identifiant d'un objet dans une liste */
	function rechercher_id(liste, id) {

		var index = -1;

		for (let i = 0; i < liste.length; i++) {
			if (liste[i].id == id) {
				index = i;
				break;}
		}
		return index;
	}


	// Si le 2ème morceau d'écharpe est trouvée
	function condition_fin(marker, indexObjet) {
		if (marker.options.nom == "Second morceau d'écharpe") {
			// afficher le message final
			document.getElementById('mapid').style.display = 'none';
			document.getElementById('images').style.display = 'none';
			document.getElementById('fin').style.display = 'block';
			document.getElementById('texte_fin').innerHTML = marker.options.message;

			map.remove();

			charger_text('fin', pseudo);
			/* Afficher dans le HTML les 3 meilleurs scores */
			charger_text('score', "");
			return true;
		}
		return false;
	}

	// Gérer un objet qui est bloqué par un autre objet
	function objet_bloquer_par_objet(marker, objet, indexObjet) {

		// on a l'objet qui débloque :
		for (let i in recuperable) {
			if (recuperable[i].id == objet.bloque_par) {

				// mettre à jour le HTML
				effacer_img_HTML(recuperable[i]);
				// supprimer l'objet de la liste récupérable
				recuperable.splice(i, 1);

				popup_text(marker, objet.message, false);

				if (condition_fin(marker, indexObjet)) {
					console.log('youpi c est fini')
					fin = true;
					return;
				}

				/* afficher la suite des objets */
				if (sauvegarde[indexObjet].ouvert == 0) {
					charger('libere', objet.id);
				}

				sauvegarde[indexObjet].ouvert = 1;
			}
		}
		// on a pas encore l'objet qui débloque :
		if (sauvegarde[indexObjet].ouvert == 0) {
			// Vérifier s'il est téléchargé
			var telecharger = rechercher_id(sauvegarde, objet.bloque_par);
			// Sinon appeler l'API
			if (telecharger == '-1') {
				charger('id', objet.bloque_par);
			}
			popup_text(marker, objet.information, true);
			click_bouton_indice(marker);
		}
	}

	// Gérer un objet bloqué par un code
	function objet_bloquer_par_code(e, marker, objet, indexObjet) {

		// Vérifier si le code est téléchargé
		var id = rechercher_id(sauvegarde, objet.bloque_par);

		// Sinon appeler l'API
		if (id != -1) {
			var code = sauvegarde[id].message;

			// Création d'un formulaire pour taper le code
			// Vérification ou non des données envoyées lors de la validation
			taper_code(e, code, indexObjet);
		}
		else {
			popup_text(marker, objet.information, true);
			click_bouton_indice(marker);
			charger('id', objet.bloque_par);
		}
	}

	// Afficher une image dans le HTML
	function afficher_img_HTML(objet) {

		document.getElementById('images').innerHTML += '<img id="'
				+ objet.nom +'" class="img" src="'+ objet.icon.options.iconUrl
				+'" height=20% alt='+objet.nom+'>';

	}

	// Effacer une image du HTML
	function effacer_img_HTML(objet) {

		var image = document.getElementById(objet.nom);
		image.parentNode.removeChild(image);
	}





	/* Gérer l'évènement clique sur un objet */
	function onObjectClick(e) {

		// Récupérer l'objet et le marqueur
		marker = e.sourceTarget.dragging._marker
		objet = e.sourceTarget.dragging._marker.options;
		// S'il est présent dans sauvegarde
		indexObjet = rechercher_id(sauvegarde, objet.id);

		// Si l'objet est déjà ouvert le signaler par un Popup
		if ((sauvegarde[indexObjet].ouvert && objet.type == 30)
			|| (sauvegarde[indexObjet].ouvert && objet.type == 40)) {
				popup_text(marker, objet.message, false);
		}

		else {
			// Si objet récupérable :
			if (objet.type == 10) {
				// déplacement dans objets récuperables
				recuperable.push(objet);
				// le mettre dans le html
				afficher_img_HTML(objet);
				// ne plus le voir sur la carte (supprimer de sauvegarde) + l'afficher dans le HTML
				if (indexObjet != -1) {
					sauvegarde.splice(indexObjet, 1);
					groupe.removeLayer(marker);
				}
			}


			// si objet code : afficher le code
			if (objet.type == 20) {

				popup_text(marker, objet.information, false);
				// Afficher les objets à libérer
				if (sauvegarde[indexObjet].ouvert == 0) {
					charger('libere', objet.id);
				}
				sauvegarde[indexObjet].ouvert = 1;
			}


			// si objet bloqué par autre objet :
			if (objet.type == 30) {
				objet_bloquer_par_objet(marker, objet, indexObjet);
			}

			// si objet bloqué par un code :
			if (objet.type == 40) {
				objet_bloquer_par_code(e, marker, objet, indexObjet);
			}
		}
	}

})
